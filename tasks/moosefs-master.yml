---
#=============================================================================
# Installation
#=============================================================================

- name: Install MooseFS master
  become: true
  package:
    name: moosefs-master
    state: present

#=============================================================================
# Generate mfsmaster.cfg
#=============================================================================

- name: Load default configuration settings for MooseFS masters
  include_vars:
    file: defaults/mfsmaster.cfg.yml
    name: infra_moosefs_master_config_defaults

- name: Update MooseFS master main configuration file
  become: true
  template:
    src: mfsmaster.cfg.j2
    dest: "{{ infra_moosefs_master_config_path }}"
    owner: root
    group: >-
      {{ infra_moosefs_master_config_settings['WORKING_GROUP'] | default(
      infra_moosefs_master_config_defaults['WORKING_GROUP']['default']) }}
    mode: 0640
  notify: Reload MooseFS master

#=============================================================================
# Generate mfsexports.cfg
#=============================================================================

- name: Load default options for MooseFS exports
  include_vars:
    file: defaults/mfsexports.cfg.options.yml
    name: infra_moosefs_master_exports_defaults

- name: Update MooseFS master exports file
  become: true
  template:
    src: mfsexports.cfg.j2
    dest: >-
      {{ infra_moosefs_master_config_settings['EXPORTS_FILENAME'] | default(
      infra_moosefs_master_config_defaults['EXPORTS_FILENAME']['default']) }}
    owner: root
    group: >-
      {{ infra_moosefs_master_config_settings['WORKING_GROUP'] | default(
      infra_moosefs_master_config_defaults['WORKING_GROUP']['default']) }}
    mode: 0640
  notify: Reload MooseFS master

#=============================================================================
# Generate mfstopology.cfg
#=============================================================================

- name: Update MooseFS master topology config file
  become: true
  template:
    src: mfstopology.cfg.j2
    dest: >-
      {{ infra_moosefs_master_config_settings['TOPOLOGY_FILENAME'] | default(
      infra_moosefs_master_config_defaults['TOPOLOGY_FILENAME']['default']) }}
    owner: root
    group: >-
      {{ infra_moosefs_master_config_settings['WORKING_GROUP'] | default(
      infra_moosefs_master_config_defaults['WORKING_GROUP']['default']) }}
    mode: 0640
  notify: Reload MooseFS master

#=============================================================================
# Initialize metadata storage
#=============================================================================

- name: Initialize metadata storage path
  become: true
  file:
    state: directory
    path: >-
      {{
      infra_moosefs_master_config_settings['DATA_PATH'] | default(
      infra_moosefs_master_config_defaults['DATA_PATH']['default'])
      }}
    owner: >-
      {{
      infra_moosefs_master_config_settings['WORKING_USER'] | default(
      infra_moosefs_master_config_defaults['WORKING_USER']['default'])
      }}
    group: >-
      {{
      infra_moosefs_master_config_settings['WORKING_GROUP'] | default(
      infra_moosefs_master_config_defaults['WORKING_GROUP']['default'])
      }}
    mode: 0750

#=============================================================================
# Service and firewall configuration
#=============================================================================

- name: Configure the MooseFS master service
  become: true
  service:
    name: moosefs-master
    enabled: "{{ infra_moosefs_master_enabled }}"
    state: "{{ 'started' if infra_moosefs_master_enabled else 'stopped' }}"

- name: Open firewall ports for the MooseFS master
  become: true
  when: ansible_os_family == 'RedHat'
  firewalld:
    immediate: true
    permanent: true
    state: enabled
    port: "{{ port }}"
  loop_control:
    loop_var: port
  with_items: >-
    {%- set ports = [] -%}
    {%- for setting in ['MATOML_LISTEN_PORT', 'MATOCS_LISTEN_PORT',
    'MATOCL_LISTEN_PORT'] -%}
      {%- if setting in infra_moosefs_master_config_settings -%}
        {{
          ports.append(infra_moosefs_master_config_settings[setting] ~ "/tcp")
        }}
      {%- else -%}
        {{
          ports.append(
            infra_moosefs_master_config_defaults[setting]['default'] ~ "/tcp")
        }}
      {%- endif -%}
    {%- endfor -%}
    {{ ports }}
