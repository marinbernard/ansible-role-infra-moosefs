# Ansible role infra/moosefs

Deploy and configure MooseFS clusters.

## Role variables
### Overview
The role relies on many variables, which are dociumented in this section.
All variables begin with the role prefix ``infra_moosefs_``. Furthermore,
each variable embeds a contextual infix showing which part of the role it
relates to. Here is the list of those contextual prefixes:

  * ``infra_moosefs_master_*``  
    These variables deal with the setup and configuration of the MooseFS
    **master** component.

  * ``infra_moosefs_chunkserver_*``  
    These variables deal with the setup and configuration of the MooseFS
    **chunkserver** component.

  * ``infra_moosefs_cgi_pages_*``  
    These variables deal with the setup and configuration of the set of
    **CGI pages** dedicated to the web supervision of a MooseFS cluster.

  * ``infra_moosefs_cgi_server_*``  
    These variables deal with the setup and configuration of the MooseFS
    **CGI server** component.

  * ``infra_moosefs_metalogger_*``  
    These variables deal with the setup and configuration of the MooseFS
    **metalogger** component.

  * ``infra_moosefs_client_*``  
    These variables deal with the setup and configuration of the MooseFS
    **client** component.

  * ``infra_moosefs_admintools_*``  
    These variables deal with the setup and configuration of the MooseFS
    **administration tools**.

### MooseFS master component
#### ``infra_moosefs_master_present`` _(bool)_  
This variable controls the installation and configuration of the MooseFS
master component. Please note that the master won't be started nor enabled
unless the ``infra_moosefs_master_enabled`` variable is also set to ``true``.

**Default value**: ``false``

------------------------------------------------------------------------------

#### ``infra_moosefs_master_enabled`` _(bool)_  
Whether to enable and start the MooseFS master daemon. This variable has no
effect unless the ``infra_moosefs_master_present`` is also set to ``true``.

**Default value**: ``true``

------------------------------------------------------------------------------

#### ``infra_moosefs_master_config_path`` _(string)_  
The full path to the _mfsmaster.cfg_ config file. Paths to other configuration
files are read from the ``infra_moosefs_master_config_settings`` variable,
or from default mfsmaster.cfg settings if no custom paths were specified.

**Default value**: ``/etc/mfs/mfsmaster.cfg``

------------------------------------------------------------------------------

#### ``infra_moosefs_master_config_settings`` _(dictionary)_  
A set of options used to build the _mfsmaster.cfg_ config file of the master
instance. This variable is a dictionary, which may include one key per legal
_mfsmaster.cfg_ setting name. Key names are validated against a list of known-
good parameter names. Values are also checked to match the expected value
type for a given setting. Setting names are case sensitive. Since all valid
MooseFS settings are written in uppercases, you are required to follow
the same convention.

Below is an example of the use of this variable. It defines two configuration
settings which will be included in the main configuration file of a MooseFS
master instance:

```yml
infra_moosefs_master_config_settings:
  ADMIN_PASSWORD: "MySecretPassword"
  AUTO_RECOVERY: true
```

The role is aware of the default values of all possible settings, and stores
this information in a separate data structure. As a consequence, the default
value of the ``infra_moosefs_master_config_settings`` variable is an empty
dictionary. Any setting which is not explicitly defined by the user will
be rendered to its default value.

**Default value**: ``{}``

------------------------------------------------------------------------------

#### ``infra_moosefs_master_exports`` _(list)_  
A list of exports to populate the _mfsexports.cfg_ config file. Each list
item is a dictionary representing a single export. Each dictionary must
comply with the following structure:

```yaml
ip: "<client_ip>"
path: "<exported_path>"
[options:
  <key>: <value>
  ...]
```

The ``options`` dictionary if a set of key-value pairs describing export
options. Each key must match one of the options described in the
mfsexports.cfg(5) man page. Key and option names are case sensitive, and must
be written in lowercase. If the ``options`` dictionary is omitted, the role
will let MooseFS fallback to default export options which are described
in the man page.

Export options are validated by the role. All options are supported, except
the ``ro``, ``rw``, ``readonly``and ``readwrite`` export options: these four
variations are gathered under a single ``readonly`` key which accepts a
boolean value. The example below shows a couple of exports, the first one
being read-only, and the second one read-write:

```yml
infra_moosefs_master_exports:
  - ip: "172.16.154.34/255.255.255.0"
    path: "/my_ro_path"
    options:
      readonly: true
      password: "mySuperPassword"
      mapall: "nobody:nobody"
  - ip: "192.168.1.1/24"
    path: "/my_rw_path"
    options:
      readonly: false
```
**Default value**  
The default value of this variable is shown below. It renders as a
_mfsexports.cfg_ file which is identical to the one provided with a new
MooseFS installation.
```yml
infra_moosefs_master_exports:
  - ip: "*"
    path: "/"
    options:
      readonly: false
      alldirs: true
      maproot: "0"
  - ip: "*"
    path: "."
    options:
      readonly: false
```

------------------------------------------------------------------------------

#### ``infra_moosefs_master_topology`` _(list)_
This variable is used to generate the _mfstopology.cfg_ config file. It is
structured as a list of dictionaries matching the pattern below:
```yml
infra_moosefs_master_topology:
  - ip: <ip>
    rack: <int>
  - ip: <ip>
    rack: <int>
  ...
```

Where ``ip`` maps to a host or network IP address, and ``rack`` to an rack
identifier integer.

Each list entry will be rendered as a line in the _mfstopology.cfg_ config
file. New installations of MooseFS come with a blank topology file. The role
fits this behaviour by providing a blank list as a default value for this
variable.

**Default value**: ``[]``

### MooseFS chunkserver component
#### ``infra_moosefs_chunkserver_present`` _(bool)_  
This variable controls the installation and configuration of the MooseFS
chunkserver component. Please note that the chunkserver won't be started nor
enabled unless the ``infra_moosefs_chunkserver_enabled`` variable is also
set to ``true``.

**Default value**: ``false``

------------------------------------------------------------------------------

#### ``infra_moosefs_chunkserver_enabled`` _(bool)_  
Whether to enable and start the MooseFS chunkserver daemon. This variable
has no effect unless the ``infra_moosefs_chunkserver_present`` is also set
to ``true``.

**Default value**: ``true``

------------------------------------------------------------------------------

#### ``infra_moosefs_chunkserver_config_path`` _(string)_  
The full path to the _mfschunkserver.cfg_ config file. Paths to other
configuration files are read from the
``infra_moosefs_chunkserver_config_settings`` variable, or from default
_mfschunkserver.cfg_ settings if no custom paths were specified.

**Default value**: ``/etc/mfs/mfschunkserver.cfg``

------------------------------------------------------------------------------

#### ``infra_moosefs_chunkserver_config_settings`` _(dictionary)_  
A set of options used to build the _mfschunkserver.cfg_ config file of the
chunkserver instance. This variable is a dictionary, which may include one
key per legal _mfschunkserver.cfg_ setting name. Key names are validated
against a list of known-good parameter names. Values are also validated to
match the expected value type for a given setting. Setting names are case
sensitive. Since all valid MooseFS settings are written in uppercase,
you are required to follow the same convention.

Below is an example of the use of this variable. It defines two configuration
settings which will be included in the main configuration file of a MooseFS
chunkserver instance:

```yml
infra_moosefs_chunkserver_config_settings:
  HDD_TEST_FREQ: 5
  HDD_PUNCH_HOLES: true
```

The role is aware of the default values of all possible settings, and stores
this information in a separate data structure. As a consequence, the default
value of the ``infra_moosefs_chunkserver_config_settings`` variable is an
empty dictionary. Any setting which is not explicitly defined by the user will
be rendered to its default value.

**Default value**: ``{}``

------------------------------------------------------------------------------

#### ``infra_moosefs_chunkserver_storage`` _(list)_
This variable stores a list of mount points serving as storage for the
MooseFS chunk server. Each list entry will be rendered to a single line in
the _mfshdd.cfg_ config file. This entry is a dictionary comprising only
two keys. The structure if this dictionary is shown below:

```yml
infra_moosefs_chunkserver_storage:
  - path: <string>
    [retired: <bool>]
```

The ``path`` key maps to the full path to a directory dedicated to MooseFS
storage. The optional ``retired`` key allows to mark a storage point for
removal: if it is set to ``true``, the storage entry will be rendered to
a line with a leading ``*`` in _mfshdd.cfg_.

**Default value**: ``[]``

### MooseFS CGI Web UI
#### ``infra_moosefs_cgi_pages_present`` _(bool)_  
If this variable is set to ``true``, the role will install the set of CGI
pages implementing the MooseFS supervision Web UI. Note that this variable
only triggers the deployment of the CGI pages: the CGI server may be deployed
by setting the ``infra_moosefs_cgi_server_present`` variable to ``true``.

**Default value**: ``false``

------------------------------------------------------------------------------

#### ``infra_moosefs_cgi_server_present`` _(bool)_  
This variable controls the installation and configuration of the MooseFS
CGI server component. Please note that the CGI server won't be started nor
enabled unless the ``infra_moosefs_cgi_server_enabled`` variable is also
set to ``true``.

Since the CGI server requires the presence of the MooseFS CGI pages, setting
this variable to ``true`` will also deploy the ``moosefs-cgi`` package.

**Default value**: ``false``

### MooseFS metalogger component
#### ``infra_moosefs_metalogger_present`` _(bool)_  
This variable controls the installation and configuration of the MooseFS
metalogger component. Please note that the metalogger won't be started nor
enabled unless the ``infra_moosefs_metalogger_enabled`` variable is also
set to ``true``.

**Default value**: ``false``

------------------------------------------------------------------------------

#### ``infra_moosefs_metalogger_enabled`` _(bool)_  
Whether to enable and start the MooseFS metalogger daemon. This variable
has no effect unless the ``infra_moosefs_metalogger_present`` is also
set to ``true``.

**Default value**: ``true``

------------------------------------------------------------------------------

#### ``infra_moosefs_metalogger_config_path`` _(string)_  
The full path to the _mfsmetalogger.cfg_ config file.

**Default value**: ``/etc/mfs/mfsmetalogger.cfg``

------------------------------------------------------------------------------

#### ``infra_moosefs_metalogger_config_settings`` _(dictionary)_  
A set of options used to build the _mfsmetalogger.cfg_ config file of the
metalogger instance. This variable is a dictionary, which may include one
key per legal _mfsmetalogger.cfg_ setting name. Key names are validated
against a list of known-good parameter names. Values are also checked
to match the expected value type for a given setting. Setting names are case
sensitive. Since all valid MooseFS settings are written in uppercase,
you are required to follow the same convention.

Below is an example of the use of this variable. It defines custom
configuration settings which will be included in the main configuration
file of a MooseFS metalogger instance:

```yml
infra_moosefs_metalogger_config_settings:
  MASTER_HOST: "mymaster.mydomain"
  BACK_LOGS: 150
```

The role is aware of the default values of all possible settings, and stores
this information in a separate data structure. As a consequence, the default
value of the ``infra_moosefs_metalogger_config_settings`` variable is an
empty dictionary. Any setting which is not explicitly defined by the user will
be rendered to its default value.

**Default value**: ``{}``

### MooseFS client component
#### ``infra_moosefs_client_present`` _(bool)_  
This variable controls the installation and configuration of the MooseFS
client component.

**Default value**: ``false``

------------------------------------------------------------------------------

#### ``infra_moosefs_client_config_path`` _(string)_
This variable set the path to the directory hosting MooseFS configuration
files for the _mfsmount_ client. These files follow the general rules
exposed in the mfsmount.cfg(5) man page. The role will generate one config
file per entry in the ``infra_moosefs_client_config_mountfiles`` variable.
These files will be stored at the location specified by this variable.

**Default value**: ``/etc/mfs``

------------------------------------------------------------------------------

#### ``infra_moosefs_client_config_mountfiles`` _(list)_
This variable is a list of MooseFS mount point definitions. Each definition
will be rendered as a single ``mfsmount-<name>.cfg`` config file, where
``<name>`` is the value of the ``config.name`` key from the definition
dictionary.

This variable is structured as follows:

```yml
infra_moosefs_client_config_mountfiles:
  - path: <string>
    config:
      name: <string>
      [owner: <string>]
      [group: <string>]
      [mode: <string>]
      [create_mountpoint: <bool>]
    [options: {}]
```

Each list item is a dictonary known as a _mount definition_. Only two keys are
mandatory within this dictionary:

  1. ``path``, which maps to the location at which the MooseFS file system
     should be mounted.

  2. ``config.name`` which maps to a string storing the unique name of the
     mount definition. This information will be used to construct the name
     of the configuration file, based on the following pattern:
     ``mfsmount-<name>.cfg``.

The dictionary may also include a bunch of optional keys:

  * ``config.owner`` allows to specify the owner of the configuration file.
    The default value is ``root``.

  * ``config.group`` allows to specify the group of the configuration file.
    The default value is ``root``.

  * ``config.mode`` allows to specify the mode of the configuration file.
    The default value is ``0640``.

  * ``config.create_mountpoint`` instructs the role to create a directory
    at ``path`` if it does not exist. Default is not to create it.

  * ``options`` is a dictionary which allows to pass custom mount options
    to be embedded into the configuration file. The role supports any option
    documented in the mfsmount(8) man page. Each key of the dictionary is
    named after an option name, and maps to the wished value for this option.

**Default value**: ``[]``

### MooseFS administration tools
#### ``infra_moosefs_admintools_present`` _(bool)_  
This variable controls the installation of MooseFS administration tools.

**Default value**: ``false``

## License
MIT

## Author
Marin Bernard <marin@olivarim.com>
